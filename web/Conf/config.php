<?php
return array(
		//数据库配置
		'DB_TYPE'=> 'mysql', // 数据库类型
		'DB_HOST'=> '192.168.4.210', // 数据库朋务器地址
		'DB_NAME'=>'weixin', // 数据库名称 
		'DB_USER'=>'root', // 数据库用户名 
		'DB_PWD'=>'a123456', // 数据库密码 
		'DB_PORT'=>'3306', // 数据库端口 
		'DB_PREFIX'=>'th_', // 数据表前缀

		//分组配置
		'APP_GROUP_LIST' => 'Home,Admin,Api',//项目分组设定
		'DEFAULT_GROUP'  => 'Admin', //默认分组
		'APP_GROUP_MODE' => 1,//开启独立分组
		'APP_GROUP_PATH' => 'Modules',//独立分组路径

		//调试配置
		'APP_STATUS' => 'debug', //应用调试模式状态
		'SHOW_PAGE_TRACE'      => false, //显示页面Trace信息

		//URL配置
		'URL_MODEL'=>3,//URL普通模式
		'URL_PATHINFO_DEPR' => '/',
		'URL_CASE_INSENSITIVE' =>true,//URL不区分大小写

		//模版配置
		'DEFAULT_THEME'=>'default',//默认模版目录
		'LAYOUT_ON' => true,
		'LAYOUT_NAME' => 'layout',

		//语音包配置
		'LANG_SWITCH_ON'=>true,
		'DEFAULT_LANG'=>'zh-cn',
		'LANG_AUTO_DETECT'=>false,
		//'LANG_LIST'=>'en-us,zh-cn,zh-tw',

		//别名配置
		'TMPL_PARSE_STRING'  =>array(
			'__PUBLIC__' => '/PUBLIC', // 更改默认的__PUBLIC__ 替换规则
			'__JS__' => '/Public/JS', // 增加新的JS类库路径替换规则
			'__UPLOAD__' => './Uploads', // 增加新的上传路径替换规则
		),

		//REST配置
		'REST_METHOD_LIST' => 'get,post,put,delete', // 允许的请求类型列表
	    'REST_DEFAULT_METHOD' => 'get', // 默认请求类型
	    'REST_CONTENT_TYPE_LIST' => 'html,xml,json,rss', // REST允许请求的资源类型列表
	    'REST_DEFAULT_TYPE' => 'html', // 默认的资源类型
	    'REST_OUTPUT_TYPE' => array(  // REST允许输出的资源类型列表
            'xml' => 'application/xml',
            'json' => 'application/json',
            'html' => 'text/html',
            'jsonp' => 'application/javascript',
			'php' => 'text/plain',
			'csv' => 'application/csv'
        ),
        'URL_ROUTER_ON'   => true, //开启路由
	    'URL_ROUTE_RULES' => array( //定义路由规则
	    	
	    ),
	    //自动加载
		//"LOAD_EXT_FILE"=>"RestClient",
);
?>