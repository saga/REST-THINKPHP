<?php
class CommonAction extends Action{
    public $api;
    public function _initialize(){
    	load("@.Admin.RestClient");
    	$api = new RestClient(array(
              'base_url' => C('BASE_URL'), 
              'format' => C('FORMAT'),
        ));
        $this->api = $api;
    }
}