<?php
class CommonAction extends Action{
    public $_params;
    public function _initialize(){
    	$applications = array( //randomly generated app key
            'home' => '28e336ac6c9423d946ba02d19c6a2632',  //前端
            'admin' => '28e336ac6c9423d946ba02d19c6a2633', //后台
            'iphone' => 'e0145e19f4d29449da2c6614f2c864ad',  //手机
      	);
      	$enc_request = $_REQUEST['enc_request'];
      	$app_id = $_REQUEST['app_id'];
      	if( !isset($applications[$app_id]) ) {
        	$msg['msg'] = 'Application does not exist!';
        	echo $this->response($msg,'json','404');
        	die();
      	}
      	$param = json_decode(authcode($enc_request,'DECODE',$applications[$app_id]));
      	$this->_params = $param;
    }
}